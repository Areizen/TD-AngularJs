var app = angular.module('App', ['ngRoute','AppController']);
app.config(['$routeProvider',
    function($routeProvider) {

        // Système de routage
        $routeProvider
        .when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'homeCtrl'
        })
        .when('/about', {
            templateUrl: 'partials/about.html',
            controller: 'aboutCtrl'
        })
        .otherwise({
          redirectTo:'/home'
        });
    }
]);
