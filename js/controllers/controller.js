var AppControllers = angular.module('AppController', []);

// Contrôleur de la page d'accueil
AppControllers.controller('homeCtrl', ['$scope','$http',function ($scope, $http){
  $http.get("https://data.rennesmetropole.fr/api/records/1.0/search//?dataset=parcours-des-lignes-de-bus-du-reseau-star")
    .then(function(response, status){
      var max_length = 0;
      for(var i = 0; i < response.data.records.length; i++)
      {
          var data = response.data.records[i].fields;
          if( data.longueur > max_length){
            max_length = data.longueur;
          }
      }

      for(var i = 0; i < response.data.records.length; i++)
      {
          response.data.records[i].fields.width = parseInt(response.data.records[i].fields.longueur *100 / max_length );
      }
      $scope.datas = response.data;

    });
}]);

AppControllers.controller('aboutCtrl',['$scope',function($scope){
  $scope.text = "Made by Romain KRAFT <romain.kraft@protonmail.com>"
}]);
